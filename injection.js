var global_topics;
var global_related;

function addCss(fileName) {

    var head = document.head;
    var link = document.createElement("link");

    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;

    head.appendChild(link);
}

addCss('https://use.fontawesome.com/releases/v5.5.0/css/all.css');


function addStyleString(css) {
    var node = document.createElement('style');
    node.innerHTML = css;
    document.body.appendChild(node);
}

function f() {
  update();
  getTopics();
  getRelated();
    addStyleString(`
  .popup {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    
    text-decoration: none;
    border-bottom: 3px solid #e53935;
  }
  
  .goodpopup {
    bortder-bottom: 3px solid green;
  }

/* The actual popup */
.popuptext {
    color: black;
    text-align: start;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 100500;
    bottom: 125%;
    left: 50%;

  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.25);
  border: solid 1px #6b8baa;
  background-color: #ffffff;
  
  font-family: -apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif;    
    
  width: fit-content;
  height: fit-content;

  display: flex; 
  flex-direction: column; 
  align-items: flex-start;
  text-align: start;
}

/* Popup arrow */
.popuptext::after {
    height: 20px;
    content: "";
    position: absolute;
    top: 0%;
    left: 50%;
    margin-top: -10px;
    margin-left: -15px;
    width: 20px;
    background-color: white;
    transform: rotate(45deg);
    border-left: solid 1px #6b8baa;
    border-top: solid 1px #6b8baa;
    
    
    
}

.popup-items {
    color: #555;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    cursor: pointer;
    
  font-size: 17px;
  color: #2a5885;
  width: -webkit-fill-available;
}

.sub-item {
  color: rgba(42, 88, 133, 0.5);
}

.popup-items:hover {
    background-color: #f0f2f5;
}

.prompt {
   cursor: default;
}

/* Toggle this class - hide and show the popup */
.show {
    visibility: visible;
    -webkit-animation: fadeIn 0.5s;
    animation: fadeIn 0.5s;
}

.noshow {
    visibility: hidden;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}


.popup-wrapper {
min-width: 380px;
min-height: 200px;
height: wrap-content;
display: flex; 
flex-direction: column; 
align-items: flex-start;
text-align: start;
z-index: 100500;
position: absolute;
} 

.ic_radio_button_off {
  width: 20px;
  height: 20px;
  object-fit: contain;
  background-color: #000000;
}

.fas, .far {
  vertical-align: text-bottom;
  margin-right: 5px;
  margin-bottom: 1px;
}

.strip {
    border: 0.5px solid #6b8baa;
    margin: 10px 10px;
    width: -webkit-fill-available;
}

.light-item {
  color: rgba(42, 88, 133, 0.5);
}
  `);

  // const ps = document.getElementsByClassName('_article_paragraph');
  // console.log(ps);
  // console.log(ps[1]);
  // const pe = document.createElement('span');
  // ps[1].insertBefore(pe, ps[1].childNodes[1]);
  // ps[1].childNodes[1].textContent = ' полуторное';
  // pe.onclick = () => document.getElementById("myPopup").classList.toggle("show");
  //
  // pe.classList.add('popup');
  // pe.id = 'fucking-pe';
  //
  // const popup_itself = `
  //           <div class="popuptext" id="myPopup">
  //               <div class="popup-items" onclick="document.getElementById('fucking-pe').childNodes[0].textContent = ' полтарашковый '; console.log('replaced');" >
  //                   <span class="far fa-circle"></span>
  //                   полтарашковый
  //               </div>
  //               <div class="popup-items" onclick="alert('option 2')" >
  //                   <span class="fas fa-undo"></span>
  //                   кековый <span class="light-item">– исходный вариант</span>
  //               </div>
  //               <div class="strip"></div>
  //               <div class="popup-items" onclick="alert('skip option')" >
  //                   <span class="fas fa-minus"></span>
  //                   <span class="light-item">пропустить</span>
  //               </div>
  //           </div>
  //   `;

// const button = `<button class="flat_button" style="
//     left: 600px;
//     top: 100px;
//     position: absolute;
//     z-index: 10;
//     ">Обработать</button>`;
}

window.onload = f;


render = function(data, status) {
    data = JSON.parse(data);
    par = document.getElementsByClassName('article_editor_canvas')[0];
    while (par.firstChild) {
        par.removeChild(par.firstChild);
    }
    console.log(document.getElementsByClassName('article_paragraph').length);
    lastTagId = -1;
    for (i = 0; i < data.length; i++) {
        const datum = data[i];
        if (datum["private"]["tagId"] !== lastTagId) {
            tag = document.createElement(datum["private"]["tagName"]);
            tag.setAttribute("class", datum["private"]["classes"]);
            tag.spellcheck = false;
            par.appendChild(tag);
            lastTagId = datum["private"]["tagId"];
        }
        if (datum["error"] !== undefined) {
            elem = document.createElement('span');
            elem.id = 'fucking-span' + i;
            elem.classList.add('popup');
            elem.append(datum["original"]);
            tag.appendChild(elem);

            const popup_name = 'popup-wrapper' + i;

            const fucking_f = (variant) => elem.childNodes[0].textContent = variant;

            var new_popup = `<div class="popuptext noshow" id="` + popup_name + `">`;


            if (datum['variants'].length === 0 || !datum['variants'][0]) {


                new_popup += `<div class="popup-items" onclick="
                        document.getElementById('fucking-span` + i + `').childNodes[0].textContent = '';
                        document.getElementById('popup-wrapper` + i + `').classList.add('noshow');
                        document.getElementById('fucking-span` + i + `').classList.remove('popup');
                        document.getElementById('fucking-span` + i + `').classList.add('goodpopup');                        
                    " >
                        <span class="far fa-trash-alt"></span>
                        удалить<span class="light-item"> – ` + datum["error"].toLowerCase() + `</span>        
                </div>`;
            } else {
                for (var k = 0; k < datum['variants'].length; k++) {
                    const hren = datum['variants'][k];

                    new_popup += `<div class="popup-items" onclick="
                        document.getElementById('fucking-span` + i + `').childNodes[0].textContent = '` + hren + `';
                        document.getElementById('popup-wrapper` + i + `').classList.add('noshow');
                        document.getElementById('fucking-span` + i + `').classList.remove('popup');
                        document.getElementById('fucking-span` + i + `').classList.add('goodpopup');                        
                    " >
                                <span class="far fa-circle"></span>
                                ` + hren + `</div>`;
                    // new_popup +=
                    //     `
                    //     <div class="popup-items" onclick="" >
                    //     <span class="far fa-circle"></span>`
                    //              + hren +
                    //     `</div>
                    //     `;

                }
            }


            new_popup += `<div class="strip"></div>`;

            new_popup += `<div class="popup-items" onclick="
                        document.getElementById('popup-wrapper` + i + `').classList.add('noshow');
                        document.getElementById('fucking-span` + i + `').classList.remove('popup');
                        document.getElementById('fucking-span` + i + `').classList.add('goodpopup');                        
                    " >
                        <span class="fas fa-minus"></span>
                        <span class="light-item">пропустить</span>        
                </div>`;

            new_popup += `</div>`;

            // const popup_itself = `
            //         <div class="popuptext noshow" id="` + popup_name + `">
            //             <div class="popup-items" onclick="" >
            //                 <span class="far fa-circle"></span>
            //                 полтарашковый
            //             </div>
            //             <div class="popup-items" onclick="alert('option 2')" >
            //                 <span class="fas fa-undo"></span>
            //                 кековый <span class="light-item">– исходный вариант</span>
            //             </div>
            //             <div class="strip"></div>
            //             <div class="popup-items" onclick="alert('skip option')" >
            //                 <span class="fas fa-minus"></span>
            //                 <span class="light-item">пропустить</span>
            //             </div>
            //         </div>
            // `;

            elem.onclick = () => {
                document.getElementById(popup_name).classList.toggle("show");
                document.getElementById(popup_name).classList.toggle("noshow");
            };

            document.getElementsByClassName('article_editor_canvas')[0].insertAdjacentHTML('afterend', new_popup);

            popup = document.getElementById(popup_name);
            popupHeight = popup.offsetHeight;
            popupWidth = popup.offsetWidth;
            shift = elem.offsetWidth / 2;

            document.getElementById(popup_name).style.left = (elem.offsetLeft - popupWidth / 2 + shift) + "px";
            document.getElementById(popup_name).style.top = (elem.offsetTop + elem.offsetHeight + 15) + "px";
        } else {
            tag.append(datum["original"]);
        }
    }

    console.log('setting the timer for 3 secs');
    window.setTimeout(() => {
        if (global_topics.length === 0) return;
        const pps = document.getElementsByClassName('_article_paragraph');
        for (var i = 0; i < (pps.length <= 3 ? pps.length : 3); i++) {
            if (pps[i].tagName !== 'P') continue;
            for (var j = 0; j < global_topics.length; j++) {
                const t = global_topics[j].toLowerCase();
                const url = 'https://vk.com/search?c%5Bper_page%5D=40&c%5Bq%5D=%23' + encodeURIComponent(t) + '&c%5Bsection%5D=statuses';

                pps[i].innerHTML = '#<strong><a href="' + url + '">' + t + '</a></strong> ' + pps[i].innerHTML;
            }
            break;
        }
    }, 10);

    console.log('setting the time for extra reading for 3 secs');
    window.setTimeout(() => {
        if (global_related.length === 0) return;
        var html = `<ul class="_article_paragraph article_paragraph article_decoration_first article_decoration_last"><li>rtrb</li>`;
        html += `<h2><strong>Читайте также на ПостНауке:</strong></h2>`;
        for (var i = 0; i < global_related.length; i++) {
            html += `<li>`;
            html += `<a href="` + global_related[i]['url'] + `">` + global_related[i]['title'] + `</a>`;
            html += `</li>`;
        }

        html += `</ul>`;

        const pps = document.getElementsByClassName('_article_paragraph');
        pps[pps.length - 1].insertAdjacentHTML('afterEnd', html);
    }, 10);

};



renderTopics = function(topics) {
    // TODO
    topics = JSON.parse(topics);
    console.log(topics);

    global_topics = topics;

    // window.setTimeout(() => {
    //     if (topics.length === 0) return;
    //     const pps = document.getElementsByClassName('_article_paragraph');
    //     for (var i = 0; i < pps.length; i++) {
    //         if (pps[i].tagName !== 'P') continue;
    //         for (var j = 0; j < topics.length; j++) {
    //             const t = topics[j].toLowerCase();
    //             const url = 'https://vk.com/search?c%5Bper_page%5D=40&c%5Bq%5D=%23' + encodeURIComponent(t) + '&c%5Bsection%5D=statuses';
    //
    //             pps[i].innerHTML = '#<strong><a href="' + url + '">' + t + '</a></strong> ' + pps[i].innerHTML;
    //         }
    //         break;
    //     }
    // }, 20);




    // return;
    // document.getElementsByClassName('article_editor_canvas')[0].childNodes[0].insertAdjacentHTML('afterEnd',
    //     `<h3 class="_article_paragraph article_paragraph article_decoration_first article_decoration_last">#<a href="#">Петушки</a></h3>`);
    //
    // document.getElementsByClassName('article_editor_canvas')[0].innerHTML +=
    //     `<h3 class="_article_paragraph article_paragraph article_decoration_first article_decoration_last">`;
    // for (var i = 0; i < topics.length; i++) {
    //   const t = topics[i].toLowerCase();
    //     document.getElementsByClassName('article_editor_canvas')[0].innerHTML += `#` + `<a href="` + url + `">` + t + `</a>` + ' ';
    // }
    // document.getElementsByClassName('article_editor_canvas')[0].innerHTML += `</h3>`;
};

renderRelated = function (related) {
    // TODO:
    console.log(related);
    global_related = JSON.parse(related);
};

function update() {
    query = [];
    paragraphs = document.getElementsByClassName('article_paragraph');
    for (i = 0; i < paragraphs.length; i++) {
        p = paragraphs[i];
        query.push(
            {
                "original": p.innerText,
                "private": {
                    "tagName": p.tagName.toLowerCase(),
                    "classes": p.classList.value,
                    "tagId": i
                }
            }
        );
    }
    console.log(JSON.stringify(query));
    $.post("https://piszi-api.herokuapp.com/api/v1/piszi",
        JSON.stringify(query),
        render
    );
}

function getTopics() {
    query = "";
    paragraphs = document.getElementsByClassName('article_paragraph');
    for (i = 0; i < paragraphs.length; i++) {
        p = paragraphs[i];
        query += p.innerText + "\n";
    }
    $.post("https://piszi-api.herokuapp.com/api/v1/topics",
        query,
        renderTopics
    );
}

function getRelated() {
    query = "";
    paragraphs = document.getElementsByClassName('article_paragraph');
    for (i = 0; i < paragraphs.length; i++) {
        p = paragraphs[i];
        query += p.innerText + "\n";
    }
    $.post("https://piszi-api.herokuapp.com/api/v1/related",
        query,
        renderRelated
    );
}